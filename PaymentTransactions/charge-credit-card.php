<?php
require 'vendor/autoload.php';

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("AUTHORIZENET_LOG_FILE", "phplog");


function chargeCreditCard($data)
{

    $credit_card_amount = $data['credit_card_amount'];
    $credit_card_number = $data['credit_card_number'];
    $credit_card_expiration_month = $data['credit_card_expiration_month'];
    $credit_card_expiration_year = $data['credit_card_expiration_year'];
  /*  $credit_card_cvv = $data['credit_card_cvv'];*/
    $credit_card_first_name = $data['credit_card_first_name'];
    $credit_card_last_name = $data['credit_card_last_name'];
    $credit_card_billing_address = $data['credit_card_billing_address'];
   /* $credit_card_billing_address2 = $data['credit_card_billing_address2'];*/
    $credit_card_billing_city = $data['credit_card_billing_city'];
    $credit_card_billing_state = $data['credit_card_billing_state'];
    $credit_card_billing_zip = $data['credit_card_billing_zip'];
    $credit_card_billing_country = $data['credit_card_billing_country'];
   /* $credit_card_telephone = $data['credit_card_telephone'];*/
    $credit_card_email = $data['credit_card_email'];
  /*  $credit_card_recipient_first_name = $data['credit_card_recipient_first_name'];
    $credit_card_shipping_address = $data['credit_card_shipping_address'];
    $credit_card_shipping_address2 = $data['credit_card_shipping_address2'];
    $credit_card_shipping_city = $data['credit_card_shipping_city'];
    $credit_card_shipping_state = $data['credit_card_shipping_state'];
    $credit_card_shipping_zip = $data['credit_card_shipping_zip'];*/
    /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
    $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);


    // Set the transaction's refId
    $refId = 'ref' . time();


    // Create the payment data for a credit card
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($credit_card_number);
    $creditCard->setExpirationDate($credit_card_expiration_year."-".$credit_card_expiration_month);
    $creditCard->setCardCode("123");


    // Add the payment data to a paymentType object
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setCreditCard($creditCard);

    // Create order information
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber("10101");
//    $order->setDescription("Golf Shirts");

    // Set the customer's Bill To address
    $customerAddress = new AnetAPI\CustomerAddressType();
    $customerAddress->setFirstName($credit_card_first_name);
    $customerAddress->setLastName($credit_card_last_name);
    $customerAddress->setAddress($credit_card_billing_address);
    $customerAddress->setCity($credit_card_billing_city);
    $customerAddress->setState($credit_card_billing_state);
    $customerAddress->setZip($credit_card_billing_zip);
    $customerAddress->setCountry($credit_card_billing_country);


    // Set the customer's identifying information
    $customerData = new AnetAPI\CustomerDataType();
    $customerData->setType("individual");
    $customerData->setId("99999456654");
    $customerData->setEmail($credit_card_email);


    // Add values for transaction settings
    $duplicateWindowSetting = new AnetAPI\SettingType();
    $duplicateWindowSetting->setSettingName("duplicateWindow");
    $duplicateWindowSetting->setSettingValue("60");


    // Add some merchant defined fields. These fields won't be stored with the transaction,
    // but will be echoed back in the response.
    $merchantDefinedField1 = new AnetAPI\UserFieldType();
    $merchantDefinedField1->setName("customerLoyaltyNum");
    $merchantDefinedField1->setValue("1128836273");

    $merchantDefinedField2 = new AnetAPI\UserFieldType();
    $merchantDefinedField2->setName("favoriteColor");
    $merchantDefinedField2->setValue("blue");

    // Create a TransactionRequestType object and add the previous objects to it
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authCaptureTransaction");
    $transactionRequestType->setAmount($credit_card_amount);
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setPayment($paymentOne);
    $transactionRequestType->setBillTo($customerAddress);
    $transactionRequestType->setCustomer($customerData);
    $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
    $transactionRequestType->addToUserFields($merchantDefinedField1);
    $transactionRequestType->addToUserFields($merchantDefinedField2);

    // Assemble the complete transaction request
    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setTransactionRequest($transactionRequestType);


    // Create the controller and get the response
    $controller = new AnetController\CreateTransactionController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);


    if ($response != null) {
        // Check to see if the API request was successfully received and acted upon
        if ($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK) {
            // Since the API request was successful, look for a transaction response
            // and parse it to display the results of authorizing the card
            $tresponse = $response->getTransactionResponse();

            if ($tresponse != null && $tresponse->getMessages() != null) {
                return $tresponse->getTransId();
            } else {
                $transaction_sms = "Transaction Failed";
                return $transaction_sms;
            }
            // Or, print errors if the API request wasn't successful
        } else {
            $transaction_sms = "Transaction Failed";
            return $transaction_sms;
        }
    } else {
        $transaction_sms = "Transaction Failed1";
        return $transaction_sms;
    }
}

/*if (!defined('DONT_RUN_SAMPLES')) {
    chargeCreditCard(\SampleCode\Constants::SAMPLE_AMOUNT);
}*/
