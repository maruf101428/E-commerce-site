<?php
require_once 'includes/header.php';
$login = Session::get("customer_login");
if ($login == true) {
    header("Location:index.php");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
    $customer_login = $customer->get_customer_through_login($_POST);
}
?>
    <div class="main">
        <div class="content">
            <div class="login_panel">
                <?php
                if (isset($customer_login)) {
                    echo $customer_login;
                }
                ?>
                <h3>Existing Customers</h3>
                <p>Sign in with the form below.</p>
                <form action="" method="post">
                    <input name="user_email" placeholder="Email" type="text"/>
                    <input name="user_pass" placeholder="Password" type="password"/>
                    <div class="buttons">
                        <div>
                            <button class="grey" name="login">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
            <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
                $customer_registration = $customer->insert_customer_through_registration($_POST);
            }
            ?>
            <div class="register_account">
                <?php
                if (isset($customer_registration)) {
                    echo $customer_registration;
                }
                ?>
                <h3>Register New Account</h3>
                <form action="" method="post">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div>
                                    <input type="text" name="user_name" placeholder="Name"/>
                                </div>

                                <div>
                                    <input type="text" name="user_city" placeholder="City">
                                </div>

                                <div>
                                    <input type="text" name="user_zip" placeholder="Zip-code"/>
                                </div>
                                <div>
                                    <input type="text" name="user_email" placeholder="Email">
                                </div>
                            </td>
                            <td>
                                <div>
                                    <input type="text" name="user_address" placeholder="Address"/>
                                </div>

                                <div>
                                    <input type="text" name="user_country" placeholder="Country"/>
                                </div>

                                <div>
                                    <input type="text" name="user_phone_no" placeholder="Phone"/>
                                </div>

                                <div>
                                    <input type="text" name="user_pass" placeholder="Password"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="search">
                        <div>
                            <button class="grey" name="register">Create Account</button>
                        </div>
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
<?php include_once 'includes/footer.php'; ?>