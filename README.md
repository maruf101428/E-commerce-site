# E-commerce website as instructed by Sj Innovation

This repository contains working code sample of a e-commerce site as instructed by a leading company known as Sj Innovation.

This repo is organized into categories and common usage instruction for better understanding.


## Using the repo Code 

The samples are all completely independent and self-contained. You can analyze them to get an understanding of how a particular method works, or you can use the snippets as a starting point for your own project.

You can also run each sample directly from the command line.

## Core Features of the Project

```
    1) User can register and login.
    2) User can see list of Products(Featured/Latest/Category wise)
    3) User can Add to cart products/ Update Add to cart
    4) User can Update their Profile Information
    5) User can bue their products
    ** 6) Authorize.net is used as payment gateway
    7) User can see their transaction report after buying products
    
```


## The problems I faced during doing this projects
```

   1)The first problem I faced is with sessions. As it is the 
   first time I worked with PHP sessions.
   2) PHP header() function problems.(Can not modify headers)
   3) Authorize.net payment gateway is the only thing 
   comepletely unknown to me before starting this projects.
   So, I faced many problems in using their Api.
    
```

## What I have learned from this projects
```
   1) How to handle PHP sessions
   2) Why problems like headers can not modify occurs.(Most of the 
   cases due to space and closing php tags or UTF BOM)
   3) How to handle payment using gateway like Authorize.net
   
   
   **During this projects I took many helps from sources like:
   
   1) https://www.youtube.com/channel/UCzmI_aDsHbBXPZzabrsM7sA(Have used a starter 
   HTML, CSS templates from here) and then modifed the templates as muchs as I could have within this short time.
   
   2) For authorize.net payment gateway I followed their documentation
   and used their provided apo end points methods for sending request and getting the appropriate response.
   
   **Link: https://developer.authorize.net/api/reference/
```

## Running the Project
* Clone this repository:
```
    $ git clone https://gitlab.com/maruf101428/E-commerce-site
```


* Run the individual samples by name. For example:
```
    ** First import the database provided naming as shop.sql
    Database username: root
    Database pass: ""
    
    For Clients url: localhost/"Type here project folder name"
    For Admin url: localhost/"Type here project folder name"/admin
    Admin Username: spider
    Admin pass:1234
    You can login to website using fahmi@gmail.com and pass:1234, if you do not want to sign in.
```


### Installation Notes For Authorize.net Payment Gateway

  Note: composer.json file has been provided. You need to write a command "composer update" to get 
  the vendor files. Also you must have composer.phar to get the work done.
  
  Note: If during "composer update", you get the error "composer failed to open stream invalid argument", go to your php.ini file (present where you have installed PHP), and uncomment the following lines:
```
extension=php_openssl.dll
extension=php_curl.dll
```
On Windows systems, you also have to uncomment:
```
extension_dir = "ext"
```
Then run `composer update` again. You might have to restart your machine before the changes take effect.

### What if I'm not using Composer?
We provide a custom `SPL` autoloader. Just [download the SDK](https://github.com/AuthorizeNet/sdk-php/releases) and point to its `autoload.php` file:

```php
require 'path/to/anet_php_sdk/autoload.php';
```
