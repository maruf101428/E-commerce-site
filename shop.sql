-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 18, 2018 at 05:55 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `admin_id` int(3) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_user_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_pass` varchar(255) NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`admin_id`, `admin_name`, `admin_user_name`, `admin_email`, `admin_pass`, `level`) VALUES
(1, 'Ahmed Maruf', 'spider', 'maruf101428@gmail.com', '1234', 0);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(3) NOT NULL,
  `brand_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_title`) VALUES
(3, 'Urban Outfitters'),
(4, 'Topshop'),
(5, 'H&amp;M'),
(6, 'Uniqlo'),
(7, 'Zara'),
(8, 'COS'),
(9, 'J.Crew'),
(10, 'Nike'),
(11, 'Levis'),
(12, 'Adidas'),
(13, 'Polo Ralph Lauren'),
(14, 'Gucci'),
(15, 'Calvin Klein'),
(16, 'Tommy Hilfiger'),
(17, 'Puma'),
(18, 'Abercrombie &amp; Fitch'),
(19, 'Lacoste'),
(20, 'Versace'),
(21, 'Dolce &amp; Gabbana');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(3) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `product_id` int(3) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float(10,3) NOT NULL,
  `quantity` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `session_id`, `product_id`, `product_name`, `price`, `quantity`, `image`) VALUES
(24, 'm74f0lquefb2vv06vqsgkludiv', 6, 'Symphony v5', 5000.000, 6, 'upload/870dbc65a4.png'),
(32, '5lflh12hlv6qqct012opmkh6e8', 6, 'Symphony v5', 5000.000, 1, 'upload/870dbc65a4.png'),
(40, 's9qqjrq6kncdf385t3au7le5ot', 19, 'Stil', 500.000, 1, 'upload/0f7c1b83b7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(3) NOT NULL,
  `cat_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_title`) VALUES
(10, 'Jeans'),
(11, 'Shoes'),
(13, 'Suits'),
(14, 'Bags &amp; Handbags'),
(15, 'Saris'),
(16, 'Skirts'),
(17, 'Undergarments'),
(18, 'Jackets'),
(19, 'Gowns'),
(20, 'Shirts'),
(21, 'T-shirts'),
(22, 'Footwear');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(3) NOT NULL,
  `customer_id` int(3) NOT NULL,
  `product_id` int(3) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` int(3) NOT NULL,
  `price` float(10,3) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `customer_id`, `product_id`, `product_name`, `quantity`, `price`, `image`, `date`) VALUES
(3, 1, 6, 'Symphony v5', 5, 25000.000, 'upload/870dbc65a4.png', '2018-01-16'),
(4, 1, 7, 'Amla', 4, 200.000, 'upload/35e5f879b5.png', '2018-01-16'),
(5, 1, 6, 'Symphony v5', 7, 35000.000, 'upload/870dbc65a4.png', '2018-01-16'),
(6, 1, 6, 'Symphony v5', 7, 35000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(7, 1, 6, 'Symphony v5', 7, 35000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(8, 1, 6, 'Symphony v5', 6, 30000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(9, 1, 6, 'Symphony v5', 5, 25000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(10, 1, 6, 'Symphony v5', 8, 40000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(11, 1, 6, 'Symphony v5', 6, 30000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(12, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(13, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(14, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(15, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(16, 1, 7, 'Amla', 1, 50.000, 'upload/35e5f879b5.png', '2018-01-17'),
(17, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(18, 1, 7, 'Amla', 1, 50.000, 'upload/35e5f879b5.png', '2018-01-17'),
(19, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(20, 1, 6, 'Symphony v5', 6, 30000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(21, 1, 6, 'Symphony v5', 4, 20000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(22, 1, 6, 'Symphony v5', 5, 25000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(23, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(24, 1, 6, 'Symphony v5', 4, 20000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(25, 1, 6, 'Symphony v5', 8, 40000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(26, 1, 6, 'Symphony v5', 7, 35000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(27, 1, 6, 'Symphony v5', 2, 10000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(28, 1, 7, 'Amla', 14, 700.000, 'upload/35e5f879b5.png', '2018-01-17'),
(29, 1, 6, 'Symphony v5', 8, 40000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(30, 1, 6, 'Symphony v5', 1, 5000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(31, 1, 6, 'Symphony v5', 3, 15000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(32, 1, 6, 'Symphony v5', 10, 50000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(33, 1, 6, 'Symphony v5', 5, 25000.000, 'upload/870dbc65a4.png', '2018-01-17'),
(34, 1, 7, 'Amla', 10, 500.000, 'upload/35e5f879b5.png', '2018-01-17'),
(35, 1, 6, 'Symphony v5', 5, 25000.000, 'upload/870dbc65a4.png', '2018-01-18'),
(36, 1, 17, 'Stocksnap', 9, 111105.000, 'upload/e4b72d7b5f.jpg', '2018-01-18'),
(37, 1, 18, 'Olu-Eletu', 1, 10000.000, 'upload/fc33e99cc0.jpg', '2018-01-18'),
(38, 2, 12, 'Gez-xavier', 13, 26000.000, 'upload/526838acc8.jpg', '2018-01-18'),
(39, 2, 19, 'Stil', 6, 3000.000, 'upload/0f7c1b83b7.jpg', '2018-01-18');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(3) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `cat_id` int(3) NOT NULL,
  `brand_id` int(3) NOT NULL,
  `product_desc` text NOT NULL,
  `price` float(10,3) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `cat_id`, `brand_id`, `product_desc`, `price`, `product_image`, `product_type`) VALUES
(12, 'Gez-xavier', 11, 15, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 2000.000, 'upload/526838acc8.jpg', 0),
(13, 'Lauren-Robers', 21, 16, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 1000.000, 'upload/a35d5a970f.jpg', 1),
(14, 'Brevite', 14, 17, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 3000.000, 'upload/214f257b48.jpg', 1),
(15, 'Imani-Clovis', 11, 17, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 500.000, 'upload/88423de44c.jpg', 1),
(17, 'Stocksnap', 10, 11, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 12345.000, 'upload/e4b72d7b5f.jpg', 0),
(18, 'Olu-Eletu', 13, 20, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 10000.000, 'upload/fc33e99cc0.jpg', 0),
(19, 'Stil', 17, 9, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 500.000, 'upload/0f7c1b83b7.jpg', 0),
(20, 'Garu', 21, 19, '&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 4321.000, 'upload/252f2fd9bc.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_city` varchar(255) NOT NULL,
  `user_country` varchar(255) NOT NULL,
  `user_zip` varchar(255) NOT NULL,
  `user_phone_no` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_address`, `user_city`, `user_country`, `user_zip`, `user_phone_no`, `user_email`, `user_pass`) VALUES
(1, 'Fahmida', 'korerpara', 'sylhet', 'Bangladesh', '3210', '3456789', 'fahmi@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(2, 'Anif', 'Shibgonj', 'Sylhet', 'Bangladesh', '3210', '01723853797', 'anif@gmail.com', '5ceb1744e9c08ce20a9d1d9384f61f56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
