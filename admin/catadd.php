﻿<?php
require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Category.php';

$category = new Category();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cat_title = $_POST['cat_title'];
    $cat_insert = $category->category_insert($cat_title);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Category</h2>
            <div class="block copyblock">
                <?php
                if (isset($cat_insert)) {
                    echo $cat_insert;
                }
                ?>
                <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="cat_title" placeholder="Enter Category Name..."
                                       class="medium"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include 'includes/footer.php';