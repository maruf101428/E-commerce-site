<?php
require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Brand.php';


$brand = new Brand();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $brand_title = $_POST['brand_title'];
    $brand_insert = $brand->brand_insert($brand_title);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Category</h2>
            <div class="block copyblock">
                <?php
                if (isset($brand_insert)) {
                    echo $brand_insert;
                }
                ?>
                <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="brand_title" placeholder="Enter Brand Name..." class="medium"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include 'includes/footer.php';