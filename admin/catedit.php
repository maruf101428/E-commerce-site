<?php require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Category.php';


if (!isset($_GET['catid']) || $_GET['catid'] == NULL) {
    echo "<script>window.location = 'catlist.php';</script>";
} else {
    $id = $_GET['catid'];
}
$category = new Category();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cat_title = $_POST['cat_title'];
    $cat_update = $category->update_category_by_id($cat_title, $id);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Category</h2>
            <div class="block copyblock">
                <?php
                if (isset($cat_update)) {
                    echo $cat_update;
                }
                ?>
                <form action="" method="post">
                    <?php
                    $get_category_by_id = $category->get_category_by_id($id);
                    if ($get_category_by_id) {
                        while ($row = $get_category_by_id->fetch_assoc()) {
                            $cat_title = $row['cat_title'];
                        }
                    }
                    ?>
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="cat_title" value="<?= $cat_title ?>" class="medium"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include 'includes/footer.php';