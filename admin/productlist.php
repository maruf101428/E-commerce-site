﻿<?php
require_once 'includes/header.php';
require_once 'includes/sidebar.php';
require_once '../classes/Product.php';
require_once '../helpers/Format.php';
$product = new Product();
$fm = new Format();

if (isset($_GET['delprod'])) {
    $id = $_GET['delprod'];
    $id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['delprod']);
    $delete_product_by_id = $product->delete_product_by_id($id);
}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Product List</h2>
            <div class="block">
                <?php
                if (isset($delete_product_by_id)) {
                    echo $delete_product_by_id;
                }
                ?>
                <table class="data display datatable" id="example">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $get_all_product = $product->get_all_product();
                    if ($get_all_product) {
                        $i = 0;
                        while ($row = $get_all_product->fetch_assoc()) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?= $i; ?></td>
                                <td><?= $row['product_name'] ?></td>
                                <td><?= $row['cat_title'] ?></td>
                                <td><?= $row['brand_title'] ?></td>
                                <td><?= $fm->textShorten($row['product_desc'], 50) ?></td>
                                <td><?= $row['price'] ?></td>
                                <td><img src="<?php echo $row['product_image'] ?>" alt="" height="60px" width="100px">
                                </td>

                                <td>
                                    <?php
                                    if ($row['product_type'] == 0) {
                                        echo "Featured";
                                    } else {
                                        echo "General";
                                    }
                                    ?>
                                </td>
                                <td><a href="productedit.php?prodid=<?= $row['product_id'] ?>">Edit</a> || <a
                                            onclick="return confirm('Are you sure to delete !')"
                                            href="?delprod=<?= $row['product_id'] ?>">Delete</a></td>
                            </tr>
                        <?php }
                    } else {
                        echo "<span class='error'>Product not found !</span>";
                    } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
            $('.datatable').dataTable();
            setSidebarHeight();
        });
    </script>
<?php include 'includes/footer.php';
