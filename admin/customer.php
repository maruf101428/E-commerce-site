<?php
require 'includes/header.php';
require 'includes/sidebar.php';
$file_path = realpath(dirname(__FILE__));
require($file_path . '/../classes/Customer.php');
//require ($file_path.'/../helpers/Format.php');

$customer = new Customer();
if (!isset($_GET['customer_id']) || $_GET['customer_id'] == NULL) {
    echo "<script>window.location = 'inbox.php';</script>";
}
$id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['customer_id']);

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>User Details Profile</h2>
            <div class="block">
                <?php
                $get_customer_data_by_id = $customer->get_customer_data_by_id($id);
                if ($get_customer_data_by_id) {
                    while ($row = $get_customer_data_by_id->fetch_assoc()) {
                        ?>
                        <table class="data display datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone No</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Zip</th>
                                <th>Country</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">

                                <td><?= $row['user_name'] ?></td>
                                <td><?= $row['user_phone_no'] ?></td>
                                <td><?= $row['user_email'] ?></td>
                                <td><?= $row['user_address'] ?></td>
                                <td><?= $row['user_city'] ?></td>
                                <td><?= $row['user_zip'] ?></td>
                                <td><?= $row['user_country'] ?></td>
                            </tr>
                            </tbody>
                        </table>
                    <?php }
                } ?>


            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
            $('.datatable').dataTable();
            setSidebarHeight();
        });
    </script>
<?php include_once 'includes/footer.php';

