<?php
require '../classes/AdminLogin.php';
$admin_login = new AdminLogin();
$loginCheck = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $admin_user_name = $_POST['admin_user_name'];
    $admin_pass = $_POST['admin_pass'];
    $loginCheck = $admin_login->adminLogin($admin_user_name, $admin_pass);
}
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Admin Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen">
</head>
<body>
<div class="container">
    <section id="content">
        <form action="" method="post">
            <h1>Admin Login</h1>
            <span style="color: red;font-size: 18px;"><?php if (isset($loginCheck)) {
                    echo $loginCheck;
                } ?>
            </span>
            <div>
                <input type="text" placeholder="Username" required="" name="admin_user_name">
            </div>
            <div>
                <input type="password" placeholder="Password" required="" name="admin_pass">
            </div>
            <div>
                <input type="submit" value="Log in" name="submit">
            </div>
        </form><!-- form -->

    </section><!-- content -->
</div><!-- container -->
</body>
</html>