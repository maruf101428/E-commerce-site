<?php
require_once 'includes/header.php';
require_once 'includes/sidebar.php';
require_once '../classes/Product.php';
require_once '../classes/Brand.php';
require_once '../classes/Category.php';

$product = new Product();

if (!isset($_GET['prodid']) || $_GET['prodid'] == NULL) {
    echo "<script>window.locategoryion = 'productlist.php';</script>";
}

$id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['prodid']);
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {

    //  echo $_POST['brand_id'];
    $update_product = $product->product_update($_POST, $_FILES, $id);
}
?>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Uproductate Product</h2>
        <div class="block">
            <?php
            if (isset($update_product)) {
                echo $update_product;
            }
            ?>
            <?php
            $get_product_by_id = $product->get_product_by_id($id);
            if ($get_product_by_id) {
                while ($row = $get_product_by_id->fetch_assoc()) {
                    ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="form">
                            <tr>
                                <td>
                                    <label>Name</label>
                                </td>
                                <td>
                                    <input type="text" name="product_name" value="<?= $row['product_name'] ?>"
                                           class="medium"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Category</label>
                                </td>
                                <td>
                                    <select id="select" name="cat_id">

                                        <option>Select Category</option>
                                        <?php
                                        $category = new Category();
                                        $get_all_category = $category->get_All_Category();
                                        if ($get_all_category) {
                                            while ($value = $get_all_category->fetch_assoc()) {
                                                ?>
                                                <option value="<?= $value['cat_id'] ?>"><?= $value['cat_title'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Brand</label>
                                </td>
                                <td>
                                    <select id="select" name="brand_id">
                                        <option>Select Brand</option>
                                        <?php
                                        $brand = new Brand();
                                        $get_all_brand = $brand->get_All_Brand();
                                        if ($get_all_brand) {
                                            while ($value = $get_all_brand->fetch_assoc()) {
                                                ?>
                                                <option value="<?= $value['brand_id'] ?>"><?= $value['brand_title'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Description</label>
                                </td>
                                <td>
                        <textarea rows="10" cols="62" name="product_desc">
                            <?= $row['product_desc'] ?>
                        </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Price</label>
                                </td>
                                <td>
                                    <input type="text" name="price" value="<?= $row['price'] ?>" class="medium"/>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label>Upload Image</label>
                                </td>
                                <td>
                                    <img height="150px;" width="200px;" src="<?= $row['product_image'] ?>" alt=""></br>
                                    <input type="file" name="product_image"/>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label>Product Type</label>
                                </td>
                                <td>
                                    <select id="select" name="product_type">
                                        <option>Select Type</option>
                                        <?php
                                        if ($row['product_type'] == 0) { ?>
                                            <option selected="selected" row="0">Featured</option>
                                            <option row="1">General</option>
                                        <?php } else {
                                            ?>
                                            <option row="0">Featured</option>
                                            <option selected="selected" row="1">General</option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="submit" Value="update_product"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                <?php }
            } ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'includes/footer.php'; ?>


