﻿<?php
require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Category.php';

$category = new Category();

if (isset($_GET['del_cat'])) {
    $id = $_GET['del_cat'];

    $delete_category_by_id = $category->delete_category_by_id($id);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Category List</h2>
            <div class="block">
                <?php
                if (isset($delete_category_by_id)) {
                    echo $delete_category_by_id;
                }
                ?>
                <table class="data display datatable" id="example">
                    <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $get_category = $category->get_All_Category();

                    if ($get_category) {
                        $i = 0;
                        while ($row = $get_category->fetch_assoc()) {
                            $i++; ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row['cat_title']; ?></td>
                                <td><a href='catedit.php?catid=<?= $row['cat_id'] ?>'>Edit</a> || <a
                                            onclick="return confirm('Are you sure to delete?')"
                                            href='?del_cat=<?= $row['cat_id'] ?>'>Delete</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
            setSidebarHeight();
        });
    </script>
<?php include 'includes/footer.php';

