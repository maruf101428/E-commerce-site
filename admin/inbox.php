﻿<?php
require 'includes/header.php';
require 'includes/sidebar.php';
$file_path = realpath(dirname(__FILE__));
require($file_path . '/../classes/Cart.php');
//require ($file_path.'/../helpers/Format.php');

$cart = new Cart();
$fm = new Format();
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Inbox</h2>
            <div class="block">
                <table class="data display datatable" id="example">
                    <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Product_ID</th>
                        <th>Customer_ID</th>
                        <th>Date and Time</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Customer Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $get_ordered_product = $cart->get_ordered_product();

                    if ($get_ordered_product) {
                        $i = 0;
                        while ($row = $get_ordered_product->fetch_assoc()) {
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?= $i ?></td>
                                <td><?= $row['product_id'] ?></td>
                                <td><?= $row['customer_id'] ?></td>
                                <td><?= $fm->formatDate($row['date']) ?></td>
                                <td><?= $row['product_name'] ?></td>
                                <td><?= $row['quantity'] ?></td>
                                <td><?= $row['price'] ?></td>
                                <td><a href='customer.php?customer_id=<?= $row['customer_id'] ?>'>View Details</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
            setSidebarHeight();
        });
    </script>
<?php require 'includes/footer.php';
