<?php
require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Brand.php';

$brand = new Brand();

if (isset($_GET['del_brand'])) {
    $id = $_GET['del_brand'];

    $delete_brand_by_id = $brand->delete_brand_by_id($id);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Category List</h2>
            <div class="block">
                <?php
                if (isset($delete_brand_by_id)) {
                    echo $delete_brand_by_id;
                }
                ?>
                <table class="data display datatable" id="example">
                    <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $get_brand = $brand->get_All_Brand();

                    if ($get_brand) {
                        $i = 0;
                        while ($row = $get_brand->fetch_assoc()) {
                            $i++; ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row['brand_title']; ?></td>
                                <td><a href='brandedit.php?brandid=<?= $row['brand_id'] ?>'>Edit</a> || <a
                                            onclick="return confirm('Are you sure to delete?')"
                                            href='?del_brand=<?= $row['brand_id'] ?>'>Delete</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
            setSidebarHeight();
        });
    </script>
<?php include 'includes/footer.php';

