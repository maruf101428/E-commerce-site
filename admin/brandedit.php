<?php
require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Brand.php';

if (!isset($_GET['brandid']) || $_GET['brandid'] == NULL) {
    echo "<script>window.location = 'brandlist.php';</script>";
} else {
    $id = $_GET['brandid'];
}
$brand = new Brand();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $brand_title = $_POST['brand_title'];
    $brand_update = $brand->update_brand_by_id($brand_title, $id);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Category</h2>
            <div class="block copyblock">
                <?php
                if (isset($brand_update)) {
                    echo $brand_update;
                }
                ?>
                <form action="" method="post">
                    <?php
                    $get_brand_by_id = $brand->get_brand_by_id($id);
                    if ($get_brand_by_id) {
                        while ($row = $get_brand_by_id->fetch_assoc()) {
                            $brand_title = $row['brand_title'];
                        }
                    }
                    ?>
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="brand_title" value="<?= $brand_title ?>" class="medium"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include 'includes/footer.php';