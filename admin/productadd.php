﻿<?php
require 'includes/header.php';
require 'includes/sidebar.php';
require '../classes/Brand.php';
require '../classes/Category.php';
require '../classes/Product.php';

$product = new Product();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
    // echo $_POST['brand_id'];
    $insert_product = $product->product_insert($_POST, $_FILES);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Product</h2>
            <div class="block">
                <?php
                if (isset($insert_product)) {
                    echo $insert_product;
                }
                ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <table class="form">

                        <tr>
                            <td>
                                <label>Name</label>
                            </td>

                            <td>
                                <input type="text" name="product_name" placeholder="Enter Product Name..."
                                       class="medium">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Category</label>
                            </td>
                            <td>

                                <select id="select" name="cat_id">

                                    <option>Select Category</option>
                                    <?php
                                    $category = new Category();
                                    $get_all_category = $category->get_All_Category();
                                    if ($get_all_category) {
                                        while ($row = $get_all_category->fetch_assoc()) {
                                            ?>
                                            <option value="<?= $row['cat_id'] ?>"><?= $row['cat_title'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Brand</label>
                            </td>
                            <td>
                                <select id="select" name="brand_id">
                                    <option>Select Brand</option>
                                    <?php
                                    $brand = new Brand();
                                    $get_all_brand = $brand->get_All_Brand();
                                    if ($get_all_brand) {
                                        while ($row = $get_all_brand->fetch_assoc()) {
                                            ?>
                                            <option value="<?= $row['brand_id'] ?>"><?= $row['brand_title'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Description</label>
                            </td>
                            <td>
                                <textarea class="tinymce" name="product_desc"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Price</label>
                            </td>
                            <td>
                                <input type="text" name="price" placeholder="Enter Price..." class="medium"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <input type="file" name="product_image"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Product Type</label>
                            </td>
                            <td>
                                <select id="select" name="product_type">
                                    <option>Select Type</option>
                                    <option value="0">Featured</option>
                                    <option value="1">Non-Featured</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <!-- Load TinyMCE -->
    <script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setupTinyMCE();
            setDatePicker('date-picker');
            $('input[type="checkbox"]').fancybutton();
            $('input[type="radio"]').fancybutton();
        });
    </script>
    <!-- Load TinyMCE -->
<?php require 'includes/footer.php';


