<?php

$file_path = realpath(dirname(__FILE__));

require($file_path . '/../config/config.php');

class Database
{
    public $host = DB_HOST;
    public $user = DB_USER;
    public $pass = DB_PASS;
    public $dbname = DB_NAME;

    public $link;
    public $error;

    //By default call to connectDB for making connection to database
    public function __construct()
    {
        $this->connectDB();
    }

    private function dd()
    {
        die(var_dump($this->link->error . __LINE__));
    }

    //Call for connecting to database !!!
    private function connectDB()
    {
        $this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
        if (!$this->link) {
            $this->error = "Connection Failed" . $this->link->connect_error;
            return false;
        }
    }

    //select a specific data
    public function select_data($query)
    {
        $result = $this->link->query($query) or $this->dd();

        if ($result->num_rows > 0) {
            return $result;
        }
        return false;
    }

    //Insert a specific data
    public function insert_data($query)
    {
        $insert_row = $this->link->query($query) or $this->dd();

        if ($insert_row) {
            return $insert_row;
        }
        return false;
    }

    //Update a specific data
    public function update_data($query)
    {
        $update_row = $this->link->query($query) or $this->dd();

        if ($update_row) {
            return $update_row;
        }
        return false;
    }

    //Delete a specific data
    public function delete_data($query)
    {
        $delete_row = $this->link->query($query) or $this->dd();

        if ($delete_row) {
            return $delete_row;
        }
        return false;
    }
}