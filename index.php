<?php require_once 'includes/header.php' ?>
<?php require_once 'includes/slider.php' ?>
    <div class="main">
        <div class="content">
            <div class="content_top">
                <div class="heading">
                    <h3>Feature Products</h3>
                </div>
                <div class="clear"></div>
            </div>
            <div class="section group">
                <?php
                $get_featured_product = $product->get_featured_product();
                if ($get_featured_product) {
                    while ($row = $get_featured_product->fetch_assoc()) {
                        ?>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="details.php?proid=<?= $row['product_id'] ?>"><img height="200px"
                                                                                       src="admin/<?= $row['product_image'] ?>"
                                                                                       alt=""></a>
                            <h2><?= $row['product_name'] ?></h2>
                            <p><?= $fm->textShorten($row['product_desc'], 60) ?></p>
                            <p><span class="price">$ <?= $row['price'] ?></span></p>
                            <div class="button"><span><a href="details.php?prodid=<?= $row['product_id'] ?>"
                                                         class="details">Details</a></span></div>
                        </div>
                    <?php }
                } ?>
            </div>
            <div class="content_bottom">
                <div class="heading">
                    <h3>New Products</h3>
                </div>
                <div class="clear"></div>
            </div>
            <div class="section group">
                <?php
                $get_new_product = $product->get_new_product();
                if ($get_new_product) {
                    while ($row = $get_new_product->fetch_assoc()) {
                        ?>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="details.php?prodid=<?= $row['product_id'] ?>"><img height="200px"
                                                                                        src="admin/<?= $row['product_image'] ?>"
                                                                                        alt=""/></a>
                            <h2><?= $row['product_name']; ?></h2>
                            <p><span class="price">$ <?= $row['price'] ?></span></p>
                            <div class="button"><span><a href="details.php?prodid=<?= $row['product_id'] ?>"
                                                         class="details">Details</a></span></div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
<?php require_once 'includes/footer.php';