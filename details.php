<?php
require_once 'includes/header.php';

if (!isset($_GET['prodid']) || $_GET['prodid'] == NULL) {
    echo "<script>window.location = '404.php';</script>";
}
$id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['prodid']);
?>
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="cont-desc span_1_of_2">
                    <?php
                    $get_product_details_by_id = $product->get_product_details_by_id($id);
                    if ($get_product_details_by_id) {
                        while ($row = $get_product_details_by_id->fetch_assoc()) {
                            ?>
                            <div class="grid images_3_of_2">
                                <img src="admin/<?= $row['product_image'] ?>" alt=""/>
                            </div>
                            <div class="desc span_3_of_2">
                                <h2><?= $row['product_name'] ?></h2>
                                <div class="price">
                                    <p>Price: <span>$<?= $row['price'] ?></span></p>
                                    <p>Category: <span><?= $row['cat_title'] ?></span></p>
                                    <p>Brand:<span><?= $row['brand_title'] ?></span></p>
                                </div>

                                <div class="add-cart">
                                    <?php
                                    $login = Session::get("Custlogin");
                                    if ($login == false) {
                                        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                                            $quantity = $_POST['quantity'];
                                            $add_to_cart = $cart->add_to_cart($quantity, $id);
                                        }
                                    } else {
                                        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                                            if ($_POST['submit'] == true) {
                                                echo "For buy any product you have to login first !";
                                            }
                                        }
                                    }
                                    ?>
                                    <form action="" method="post">
                                        <input type="number" class="buyfield" name="quantity" value="1"/>
                                        <input type="submit" class="buysubmit" name="submit" value="Buy Now"/>
                                    </form>
                                </div>
                                <span style="color:red;font-size: 20px;">
						<?php if (isset($add_to_cart)) {
                            echo $add_to_cart;
                        } ?>
					</span>
                            </div>
                            <div class="product-desc">
                                <h2>Product Details</h2>
                                <?= $row['product_desc'] ?>
                            </div>
                        <?php }
                    } ?>
                </div>
                <div class="rightsidebar span_3_of_1">
                    <h2>CATEGORIES</h2>
                    <ul>
                        <?php
                        $get_all_category = $category->get_All_Category();
                        if ($get_all_category) {
                            while ($row = $get_all_category->fetch_assoc()) {
                                ?>
                                <li><a href="productbycat.php?cat_id=<?= $row['cat_id'] ?>"><?= $row['cat_title'] ?></a>
                                </li>
                            <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php include_once 'includes/footer.php';