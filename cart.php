<?php require_once 'includes/header.php';
if (isset($_GET['delprod'])) {
    $delete_id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['delprod']);
    $delete_product = $cart->delete_product_from_cart_by_id($delete_id);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cart_id = $_POST['cart_id'];
    $quantity = $_POST['quantity'];
    $update_cart_quantity = $cart->update_cart_quantity($cart_id, $quantity);
    if ($quantity <= 0) {
        $delete_product = $cart->delete_product_from_cart_by_id($cart_id);
    }
}

if (!isset($_GET['id'])) {
    echo "<meta http-equiv='refresh' content='0;URL=?id=live'/>";
}
?>
    <div class="main">
        <div class="content">
            <div class="cartoption">
                <div class="cartpage">
                    <h2>Your Cart</h2>
                    <?php
                    if (isset($update_cart_quantity)) {
                        echo $update_cart_quantity;
                    }

                    if (isset($delete_product)) {
                        echo $delete_product;
                    }
                    ?>
                    <table class="tblone">
                        <tr>
                            <th width="5%">Sl.</th>
                            <th width="30%">Producart Name</th>
                            <th width="10%">Image</th>
                            <th width="15%">Price</th>
                            <th width="15%">Quantity</th>
                            <th width="15%">Total Price</th>
                            <th width="10%">Acartion</th>
                        </tr>
                        <?php
                        $get_cart_product = $cart->get_cart_product();
                        if ($get_cart_product) {
                            $i = 0;
                            $sum = 0;
                            $qty = 0;
                            while ($row = $get_cart_product->fetch_assoc()) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $row['product_name'] ?></td>
                                    <td><img src="admin/<?= $row['image'] ?>" alt=""/></td>
                                    <td>Tk. <?= $row['price'] ?></td>

                                    <td>

                                        <form action="" method="post">
                                            <input type="hidden" name="cart_id" value="<?= $row['cart_id'] ?>"/>
                                            <input type="number" name="quantity" value="<?= $row['quantity'] ?>"/>
                                            <input type="submit" name="submit" value="Update"/>
                                        </form>

                                    </td>

                                    <td>Tk.
                                        <?php
                                        $total = $row['price'] * $row['quantity'];
                                        echo $total;
                                        ?>
                                    </td>

                                    <td><a onclick="return confirm('Are you sure to delete !');"
                                           href="?delprod=<?= $row['cart_id'] ?>">X</a></td>
                                </tr>
                                <?php
                                $qty = $qty + $row['quantity'];
                                $sum = $sum + $total;
                                ?>
                            <?php }
                        } ?>
                    </table>
                    <table style="float:right;text-align:left;" width="40%">
                        <?php
                        $check_cart_table_data = $cart->check_cart_table_data();
                        if ($check_cart_table_data) {
                            ?>
                            <tr>
                                <th>Sub Total :</th>
                                <td>TK. <?= $sum ?></td>
                            </tr>
                            <tr>
                                <th>VAT :</th>
                                <td>TK. 10%</td>
                            </tr>
                            <tr>
                                <th>Grand Total :</th>
                                <td>TK.<?php
                                    $vat = $sum * 0.10;
                                    $grand_total = $sum + $vat;
                                    echo $grand_total;
                                    ?></td>
                            </tr>
                            <?php
                            Session::set("qty", $qty);
                            Session::set("sum", $sum);
                            ?>
                        <?php } else {
                            //echo "Cart empty !";
                            header("Location:index.php");
                        } ?>
                    </table>
                </div>

                <div class="shopping">
                    <div class="shopleft">
                        <a href="index.php"> <img src="images/shop.png" alt=""/></a>
                    </div>
                    <div class="shopright">
                        <a href="submitpayment.php"> <img src="images/check.png" alt=""/></a>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

<?php require_once 'includes/footer.php';