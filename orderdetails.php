<?php
require_once 'includes/header.php';
$login = Session::get("customer_login");

if ($login == false) {
    header("Location:login.php");
}
?>
    <style>
        .tblone tr td {
            text-align: justify;
        }
    </style>
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="order">
                    <h2>Your order details</h2>
                    <table class="tblone">
                        <tr>
                            <th>No</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $customer_id = Session::get("customer_id");
                        $get_ordered_product = $cart->get_ordered_product($customer_id);

                        if ($get_ordered_product) {
                            $i = 0;
                            while ($row = $get_ordered_product->fetch_assoc()) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $row['product_name'] ?></td>
                                    <td><img src="admin/<?= $row['image'] ?>" alt=""/></td>
                                    <td><?php echo $row['quantity'] ?></td>
                                    <td>Tk. <?php
                                        $total = $row['price'] * $row['quantity'];
                                        echo $total;
                                        ?></td>
                                    <td><?= $fm->formatDate($row['date']); ?></td>
                                    <td><?php
                                        if ($row['status'] == '0') {
                                            echo "Pending";
                                        } else {
                                            echo "Shifted";
                                        }
                                        ?></td>
                                    <?php
                                    if ($row['status'] == '1') { ?>
                                        <td><a onclick="return confirm('Are you sure to delete !');" href="">X</a></td>
                                    <?php } else { ?>
                                        <td>N/A</td>
                                    <?php } ?>

                                </tr>
                            <?php }
                        } ?>
                    </table>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

<?php include_once 'includes/footer.php';