<?php

namespace SampleCode;

class Constants
{
    //merchant credentials
    const MERCHANT_LOGIN_ID = "8AexeN4g6T";
    const MERCHANT_TRANSACTION_KEY = "87Y5wt9486jnSTYV";

    const RESPONSE_OK = "Ok";

    //Recurring Billing
    const SUBSCRIPTION_ID_GET = "2930242";

    //Transaction Reporting
    const TRANS_ID = "2238968786";

    const SAMPLE_AMOUNT = "2.23";
}
