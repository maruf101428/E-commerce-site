<?php
require_once '../library/Database.php';
require_once '../helpers/Format.php';

class Brand
{
    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function brand_insert($title)
    {
        $brand_title = $this->fm->validation($title);
        $brand_title = mysqli_real_escape_string($this->db->link, $brand_title);

        if (empty($brand_title)) {
            $brand_sms = "<span class='error'>Brand Field must not be empty</span>";
            return $brand_sms;
        }
        $query = "INSERT
                  INTO
                  brand(brand_title)
                  VALUES ('{$brand_title}')";

        $brand_insert_query = $this->db->insert_data($query);

        if ($brand_insert_query) {
            $brand_sms = "<span class='success'>Brand Inserted Succesfully</span>";
            return $brand_sms;

        }
        $brand_sms = "<span class='error'>Brand NOT Inserted Succesfully</span>";

        return $brand_sms;
    }

    public function delete_brand_by_id($id)
    {
        $query = "DELETE
                  FROM
                  brand
                  WHERE 
                  brand_id = '{$id}'";

        $delete_brand_by_id = $this->db->delete_data($query);
        if ($delete_brand_by_id) {
            $brand_sms = "<span class='success'>Brand Deleted Succesfully</span>";
            return $brand_sms;
        }
        $brand_sms = "<span class='success'>Error deleting brand</span>";
        return $brand_sms;
    }


    public function get_All_Brand()
    {
        $query = "SELECT *
                  FROM
                  brand
                  ORDER BY
                  brand_id
                  DESC";

        $select_brand_query = $this->db->select_data($query);

        return $select_brand_query;
    }


    public function update_brand_by_id($brand_title, $brand_id)
    {
        $brand_title = $this->fm->validation($brand_title);
        $brand_title = mysqli_real_escape_string($this->db->link, $brand_title);
        if (empty($brand_title)) {
            $brand_sms = "<span class='error'>brand Field must not be empty</span>";
            return $brand_sms;
        } else {
            $query = "UPDATE
                      brand
                      SET
                      brand_title = '{$brand_title}'
                      WHERE 
                      brand_id = '{$brand_id}'";

            $update_brand_by_id = $this->db->update_data($query);
            if ($update_brand_by_id) {
                $brand_sms = "<span class='success'>brand Updated Succesfully</span>";
                return $brand_sms;
            }
            $brand_sms = "<span class='success'>brand Not Updated</span>";
            return $brand_sms;
        }

    }

    public function get_brand_by_id($id)
    {
        $query = "SELECT *
                  FROM
                  brand
                  WHERE
                  brand_id = '{$id}'";

        $select_brand_by_id = $this->db->select_data($query);
        return $select_brand_by_id;


    }

}