<?php
$filepath = realpath(dirname(__FILE__));
require_once($filepath . '/../library/Database.php');
require_once($filepath . '/../helpers/Format.php');

class Product
{
    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function product_insert($data, $file)
    {
        $product_name = $this->fm->validation($data['product_name']);
        $cat_id = $data['cat_id'];
        $brand_id = $data['brand_id'];
        $product_desc = $this->fm->validation($data['product_desc']);
        $price = $this->fm->validation($data['price']);
        $product_type = $this->fm->validation($data['product_type']);

        $product_name = mysqli_real_escape_string($this->db->link, $product_name);
        $product_desc = mysqli_real_escape_string($this->db->link, $product_desc);
        $price = mysqli_real_escape_string($this->db->link, $price);
        $product_type = mysqli_real_escape_string($this->db->link, $product_type);

        $allowed_image_type = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['product_image']['name'];
        $file_size = $file['product_image']['size'];
        $file_temp = $file['product_image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "upload/" . $unique_image;

        if ($product_name == "" || $cat_id == "" || $brand_id == "" || $product_desc == "" || $price == "" || $file_name == "" || $product_type == "") {
            $msg = "<span class='error'>Product field must not be empty !</span>";
            return $msg;
        } elseif ($file_size > 2048567) {
            echo "<span class='error'>Image Size should be less then 2MB!</span>";
        } elseif (in_array($file_ext, $allowed_image_type) === false) {
            echo "<span class='error'>You can upload only:-" . implode(', ', $allowed_image_type) . "</span>";
        } else {

            move_uploaded_file($file_temp, $uploaded_image);
            $query = "INSERT 
                      INTO 
                      product(product_name, cat_id, brand_id, product_desc, price, product_image, product_type)
                      VALUES('{$product_name}','$cat_id', '$brand_id', '{$product_desc}', '$price', '{$uploaded_image}', '$product_type')";

            $product_insert_query = $this->db->insert_data($query);
            if ($product_insert_query) {
                $msg = "<span class='success'>Product inserted successfully.</span>";
                return $msg;
            } else {
                $msg = "<span class='error'>Product not inserted !</span>";
                return $msg;
            }
        }
    }

    public function get_all_product()
    {
        /* $query = "SELECT
                   product.*, category.cat_title, brand.brand_title
                   FROM
                   product
                   INNER JOIN
                   category
                   ON
                   product.cat_id = category.cat_id
                   INNER JOIN
                   brand
                   ON
                   product.brand_id = brand.brand_id
                   ORDER BY
                   product.product_id
                   DESC";*/


        $query = "SELECT 
                  p.*, c.cat_title, b.brand_title
                  FROM 
                  product as p, category as c, brand as b
                  WHERE 
                  p.cat_id = c.cat_id AND p.brand_id = b.brand_id
                  ORDER BY 
                  p.product_id 
                  DESC";
        $result = $this->db->select_data($query);
        return $result;
    }


    public function get_product_by_id($id)
    {
        $query = "SELECT * 
                  FROM 
                  product 
                  WHERE 
                  product_id = '$id'";

        $result = $this->db->select_data($query);
        return $result;
    }


    public function product_update($data, $file, $id)
    {
        $product_name = $this->fm->validation($data['product_name']);
        $cat_id = $data['cat_id'];
        $brand_id = $data['brand_id'];
        $product_desc = $this->fm->validation($data['product_desc']);
        $price = $this->fm->validation($data['price']);
        $product_type = $this->fm->validation($data['product_type']);

        $product_name = mysqli_real_escape_string($this->db->link, $product_name);
        $product_desc = mysqli_real_escape_string($this->db->link, $product_desc);
        $price = mysqli_real_escape_string($this->db->link, $price);
        $product_type = mysqli_real_escape_string($this->db->link, $product_type);

        $allowed_image_type = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['product_image']['name'];
        $file_size = $file['product_image']['size'];
        $file_temp = $file['product_image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "upload/" . $unique_image;

        if ($product_name == "" || $cat_id == "" || $brand_id == "" || $product_desc == "" || $price == "" || $product_type == "") {
            $msg = "<span class='error'>Product field must not be empty !</span>";
            return $msg;
        } else {
            if (!empty($file_name)) {
                if ($file_size > 1048567) {
                    echo "<span class='error'>Image Size should be less then 1MB!</span>";
                } elseif (in_array($file_ext, $allowed_image_type) === false) {
                    echo "<span class='error'>You can upload only:-" . implode(', ', $allowed_image_type) . "</span>";
                } else {
                    move_uploaded_file($file_temp, $uploaded_image);
                    $query = "UPDATE 
                              product 
                              SET
                              product_name = '$product_name',
                              cat_id = '$cat_id',
                              brand_id = '$brand_id',
                              product_desc = '$product_desc',
                              price = '$price',
                              product_image = '$uploaded_image',
                              product_type = '$product_type'
                              WHERE 
                              product_id = '$id'";

                    $updated_rows = $this->db->update_data($query);
                    if ($updated_rows) {
                        $msg = "<span class='success'>Product updated successfully.</span>";
                        return $msg;
                    }
                    $msg = "<span class='error'>Product not updated !</span>";
                    return $msg;

                }
            } else {
                $query = "UPDATE 
                          product 
                          SET
				    	  product_name = '$product_name',
				          cat_id = '$cat_id',
				    	  brand_id = '$brand_id',
				    	  product_desc = '$product_desc',
				    	  price = '$price',
				    	  product_type = '$product_type'
				    	  WHERE 
				    	  product_id = '$id'";

                $updated_rows = $this->db->update_data($query);

                if ($updated_rows) {
                    $msg = "<span class='success'>Product updated successfully.</span>";
                    return $msg;
                }
                $msg = "<span class='error'>Product not updated !</span>";
                return $msg;
            }

        }

    }


    public function delete_product_by_id($id)
    {

        $query = "SELECT * 
                  FROM 
                  product 
                  WHERE 
                  product_id = '$id'";

        //Unlinking image from tmp file and perm file
        $get_file_data = $this->db->select_data($query);
        if ($get_file_data) {
            while ($row = $get_file_data->fetch_assoc()) {
                $del_link = $row['product_image'];
                unlink($del_link);
            }
        }


        $query = "DELETE 
                  FROM 
                  product 
                  WHERE 
                  product_id = '$id'";

        $delete_product = $this->db->delete_data($query);
        if ($delete_product) {
            $msg = "<span class='success'>Product Deleted Successfully !</span>";
            return $msg;
        }
        $msg = "<span class='error'>Product not Deleted !</span>";
        return $msg;
    }


    public function get_featured_product()
    {
        $query = "SELECT * 
                  FROM 
                  product 
                  WHERE 
                  product_type = '0' 
                  ORDER BY 
                  product_id 
                  DESC 
                  LIMIT 4";

        $result = $this->db->select_data($query);
        return $result;
    }


    public function get_new_product()
    {
        $query = "SELECT * 
                  FROM 
                  product 
                  ORDER BY 
                  product_id 
                  DESC 
                  LIMIT 4";

        $result = $this->db->select_data($query);
        return $result;
    }


    public function get_product_details_by_id($id)
    {
        $query = "SELECT 
                  p.*, c.cat_title, b.brand_title
			      FROM product as p, category as c, brand as b
			      WHERE p.cat_id = c.cat_id AND p.brand_id = b.brand_id AND p.product_id = '$id'";

        $result = $this->db->select_data($query);
        return $result;
    }


    public function get_product_by_category_id($id)
    {
        $id = mysqli_real_escape_string($this->db->link, $id);
        $query = "SELECT * 
                  FROM 
                  product 
                  WHERE 
                  cat_id = '$id'";

        $result = $this->db->select_data($query);
        return $result;
    }


}