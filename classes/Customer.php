<?php
$filepath = realpath(dirname(__FILE__));
require_once($filepath . '/../library/Database.php');
require_once($filepath . '/../helpers/Format.php');

class Customer
{
    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function insert_customer_through_registration($data)
    {
        $user_name = $this->fm->validation($data['user_name']);
        $user_address = $this->fm->validation($data['user_address']);
        $user_city = $this->fm->validation($data['user_city']);
        $user_country = $this->fm->validation($data['user_country']);
        $user_zip = $this->fm->validation($data['user_zip']);
        $user_phone_no = $this->fm->validation($data['user_phone_no']);
        $user_email = $this->fm->validation($data['user_email']);
        $user_pass = $this->fm->validation(md5($data['user_pass']));

        $user_name = mysqli_real_escape_string($this->db->link, $user_name);
        $user_address = mysqli_real_escape_string($this->db->link, $user_address);
        $user_city = mysqli_real_escape_string($this->db->link, $user_city);
        $user_country = mysqli_real_escape_string($this->db->link, $user_country);
        $user_zip = mysqli_real_escape_string($this->db->link, $user_zip);
        $user_phone_no = mysqli_real_escape_string($this->db->link, $user_phone_no);
        $user_email = mysqli_real_escape_string($this->db->link, $user_email);
        $user_pass = mysqli_real_escape_string($this->db->link, $user_pass);

        if ($user_name == "" || $user_address == "" || $user_city == "" || $user_country == "" || $user_zip == "" || $user_phone_no == "" || $user_email == "" || $user_pass == "") {
            $msg = "<span class='error'>Field must not be empty !</span>";
            return $msg;
        }

        $query = "SELECT * 
                  FROM 
                  users 
                  WHERE 
                  user_email = '$user_email' 
                  LIMIT 1";


        $mail_check = $this->db->select_data($query);
        if ($mail_check != false) {
            $msg = "<span class='error'>Mail already exist !</span>";
            return $msg;
        }
        $query = "INSERT 
                  INTO 
                  users(user_name, user_address, user_city, user_country, user_zip, user_phone_no, user_email, user_pass)
                  VALUES('$user_name','$user_address', '$user_city', '$user_country', '$user_zip', '$user_phone_no', '$user_email', '$user_pass')";

        $inserted_rows = $this->db->insert_data($query);

        if ($inserted_rows) {
            $msg = "<span class='success'>Customer data inserted successfully.</span>";
            return $msg;
        }
        $msg = "<span class='error'>Customer data not inserted !</span>";
        return $msg;

    }

    public function get_customer_through_login($data)
    {
        $user_email = $this->fm->validation($data['user_email']);
        $user_pass = $this->fm->validation(md5($data['user_pass']));

        $user_email = mysqli_real_escape_string($this->db->link, $user_email);
        $user_pass = mysqli_real_escape_string($this->db->link, $user_pass);

        if (empty($user_email) || empty($user_pass)) {
            $msg = "<span class='error'>Field must not be empty !</span>";
            return $msg;
        }

        $query = "SELECT * 
                  FROM 
                  users 
                  WHERE 
                  user_email = '$user_email' 
                  AND 
                  user_pass = '$user_pass'";

        $select_user = $this->db->select_data($query);

        if ($select_user != false) {
            $row = $select_user->fetch_assoc();
            Session::set("customer_login", true);
            Session::set("customer_id", $row['user_id']);
            Session::set("customer_name", $row['user_name']);
            header("Location:cart.php");
        }
        $msg = "<span class='error'>Email or Password not matched !</span>";
        return $msg;
    }


    public function get_customer_data_by_id($id)
    {
        $query = "SELECT * 
                  FROM
                  users 
                  WHERE 
                  user_id = '$id'";
        $result = $this->db->select_data($query);
        return $result;
    }

    public function update_customer_details($data, $customer_id)
    {
        $user_name = $this->fm->validation($data['user_name']);
        $user_address = $this->fm->validation($data['user_address']);
        $user_city = $this->fm->validation($data['user_city']);
        $user_country = $this->fm->validation($data['user_country']);
        $user_zip = $this->fm->validation($data['user_zip']);
        $user_phone = $this->fm->validation($data['user_phone_no']);
        $user_email = $this->fm->validation($data['user_email']);

        $user_name = mysqli_real_escape_string($this->db->link, $user_name);
        $user_address = mysqli_real_escape_string($this->db->link, $user_address);
        $user_city = mysqli_real_escape_string($this->db->link, $user_city);
        $user_country = mysqli_real_escape_string($this->db->link, $user_country);
        $user_zip = mysqli_real_escape_string($this->db->link, $user_zip);
        $user_phone = mysqli_real_escape_string($this->db->link, $user_phone);
        $user_email = mysqli_real_escape_string($this->db->link, $user_email);

        if ($user_name == "" || $user_address == "" || $user_city == "" || $user_country == "" || $user_zip == "" || $user_phone == "" || $user_email == "") {
            $msg = "<span class='error'>Field must not be empty !</span>";
            return $msg;
        }
        $query = "UPDATE 
                  users 
                  SET
                  user_name = '$user_name',
                  user_address = '$user_address',
                  user_city = '$user_city',
                  user_country = '$user_country',
                  user_zip = '$user_zip',
                  user_phone_no = '$user_phone',
                  user_email = '$user_email'
                  WHERE 
                  user_id = '$customer_id'";

        $updated_rows = $this->db->update_data($query);
        if ($updated_rows) {
            $msg = "<span class='success'>User Data updated successfully.</span>";
            return $msg;
        }
        $msg = "<span class='error'>User Data not updated !</span>";
        return $msg;
    }
}
