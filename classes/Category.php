<?php
$filepath = realpath(dirname(__FILE__));
require_once($filepath . '/../library/Database.php');
require_once($filepath . '/../helpers/Format.php');

class Category
{
    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function category_insert($title)
    {
        $cat_title = $this->fm->validation($title);
        $cat_title = mysqli_real_escape_string($this->db->link, $cat_title);

        if (empty($cat_title)) {
            $category_sms = "<span class='error'>Category Field must not be empty</span>";
            return $category_sms;
        }
        $query = "INSERT
                  INTO
                  category(cat_title)
                  VALUES ('{$cat_title}')";

        $category_insert_query = $this->db->insert_data($query);

        if ($category_insert_query) {
            $category_sms = "<span class='success'>Category Inserted Succesfully</span>";
            return $category_sms;

        }
        $category_sms = "<span class='error'>Category NOT Inserted Succesfully</span>";

        return $category_sms;
    }

    public function get_All_Category()
    {
        $query = "SELECT *
                  FROM
                  category
                  ORDER BY
                  cat_id
                  DESC";

        $select_category_query = $this->db->select_data($query);

        return $select_category_query;
    }

    public function get_category_by_id($id)
    {
        $query = "SELECT *
                  FROM
                  category
                  WHERE
                  cat_id = '{$id}'";

        $select_category_by_id = $this->db->select_data($query);
        return $select_category_by_id;


    }

    public function update_category_by_id($cat_title, $cat_id)
    {
        $cat_title = $this->fm->validation($cat_title);
        $cat_title = mysqli_real_escape_string($this->db->link, $cat_title);
        if (empty($cat_title)) {
            $category_sms = "<span class='error'>Category Field must not be empty</span>";
            return $category_sms;
        } else {
            $query = "UPDATE
                      category
                      SET
                      cat_title = '{$cat_title}'
                      WHERE 
                      cat_id = '{$cat_id}'";

            $update_category_by_id = $this->db->update_data($query);
            if ($update_category_by_id) {
                $category_sms = "<span class='success'>Category Updated Succesfully</span>";
                return $category_sms;
            }
            $category_sms = "<span class='success'>Category Not Updated</span>";
            return $category_sms;
        }

    }

    public function delete_category_by_id($id)
    {
        $query = "DELETE
                  FROM
                  category
                  WHERE 
                  cat_id = '{$id}'";

        $delete_category_by_id = $this->db->delete_data($query);
        if ($delete_category_by_id) {
            $category_sms = "<span class='success'>Category Deleted Succesfully</span>";
            return $category_sms;
        }
        $category_sms = "<span class='success'>Error deleting category</span>";
        return $category_sms;
    }
}