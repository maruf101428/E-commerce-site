<?php
$filepath = realpath(dirname(__FILE__));
require_once($filepath . '/../library/Database.php');
require_once($filepath . '/../helpers/Format.php');

class Cart
{
    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function add_to_cart($quantity, $id)
    {

        $quantity = $this->fm->validation($quantity);
        $quantity = mysqli_real_escape_string($this->db->link, $quantity);
        $product_id = mysqli_real_escape_string($this->db->link, $id);
        $session_id = session_id();

        $query = "SELECT * 
                  FROM 
                  product 
                  WHERE 
                  product_id = '$product_id'";


        $row = $this->db->select_data($query)->fetch_assoc();

        $product_name = $row['product_name'];
        $price = $row['price'];
        $image = $row['product_image'];

        $query = "SELECT * 
                  FROM 
                  cart 
                  WHERE 
                  product_id = '$product_id' AND session_id = '$session_id'";


        $get_product = $this->db->select_data($query);
        if ($get_product) {
            $msg = "Product already added !";
            return $msg;
        }
        $query = "INSERT 
                  INTO 
                  cart(session_id, product_id, product_name, price, quantity, image)
                  VALUES('$session_id','$product_id', '$product_name', '$price','$quantity', '$image')";

        $inserted_rows = $this->db->insert_data($query);
        // die(var_dump($inserted_rows));
        if ($inserted_rows === true) {
            header("Location:cart.php");
        } else
            header("Location:404.php");

    }


    public function get_cart_product()
    {
        $session_id = session_id();
        $query = "SELECT * 
                  FROM 
                  cart 
                  WHERE 
                  session_id = '$session_id'";

        $row = $this->db->select_data($query);
        return $row;
    }


    public function update_cart_quantity($cart_id, $quantity)
    {
        $cart_id = mysqli_real_escape_string($this->db->link, $cart_id);
        $quantity = mysqli_real_escape_string($this->db->link, $quantity);

        $query = "UPDATE
                  cart 
                  SET 
                  quantity = '$quantity' 
                  WHERE 
                  cart_id = '$cart_id'";

        $updated_row = $this->db->update_data($query);

        if ($updated_row) {
            header("Location:cart.php");
        }
        $msg = "<span class='error'>Quantity not updated !</span>";
        return $msg;
    }


    public function delete_product_from_cart_by_id($delete_id)
    {
        $query = "DELETE 
                  FROM
                  cart 
                  WHERE 
                  cart_id = '$delete_id'";

        $delete_data = $this->db->delete_data($query);

        if ($delete_data) {
            echo "<script>window.location = 'cart.php';</script>";
        }
        $msg = "<span class='error'>Product not Deleted !</span>";
        return $msg;
    }

    public function check_cart_table_data()
    {
        $session_id = session_id();
        $query = "SELECT * 
                  FROM 
                  cart 
                  WHERE 
                  session_id = '$session_id'";

        $row = $this->db->select_data($query);
        return $row;
    }


    public function delete_customer_cart()
    {
        $session_id = session_id();
        $query = "DELETE
                  FROM
                  cart 
                  WHERE 
                  session_id = '$session_id'";

        $this->db->delete_data($query);
    }

    public function order_product_by_customer($customer_id)
    {
        $session_id = session_id();
        $query = "SELECT * 
                  FROM
                  cart 
                  WHERE 
                  session_id = '$session_id'";

        $get_product = $this->db->select_data($query);

        if ($get_product) {
            while ($row = $get_product->fetch_assoc()) {
                $product_id = $row['product_id'];
                $product_name = $row['product_name'];
                $quantity = $row['quantity'];
                $price = $row['price'] * $quantity;
                $image = $row['image'];

                $query = "INSERT 
                          INTO 
                          orders(customer_id, product_id, product_name, quantity, price, image, date)
                          VALUES('$customer_id','$product_id', '$product_name', '$quantity','$price', '$image', now())";

                $inserted_rows = $this->db->insert_data($query);
            }
        }
    }


    public function payable_ammount($customer_id)
    {
        $query = "SELECT * 
                  FROM
                  orders 
                  WHERE 
                  customer_id = '$customer_id'";

        $row = $this->db->select_data($query);
        return $row;
    }


    public function get_ordered_product()
    {
        $query = "SELECT * 
                  FROM
                  orders  
                  ORDER 
                  BY 
                  product_id 
                  DESC";
        $row = $this->db->select_data($query);
        return $row;
    }


    public function check_customer_order_by_id($customer_id)
    {
        $query = "SELECT * 
                  FROM 
                  order 
                  WHERE 
                  customer_id = '$customer_id'";

        $row = $this->db->select_data($query);
        return $row;
    }


}