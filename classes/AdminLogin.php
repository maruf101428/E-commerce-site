<?php
$filepath = realpath(dirname(__FILE__));
require_once($filepath . '/../library/Session.php');
Session::checkLogin();
require_once($filepath . '/../library/Database.php');
require_once($filepath . '/../helpers/Format.php');

class AdminLogin
{

    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function adminLogin($admin_user_name, $admin_pass)
    {
        $admin_user_name = $this->fm->validation($admin_user_name);
        $admin_pass = $this->fm->validation($admin_pass);

        $admin_user_name = mysqli_real_escape_string($this->db->link, $admin_user_name);
        $admin_pass = mysqli_real_escape_string($this->db->link, $admin_pass);

        if (empty($admin_user_name) || empty($admin_pass)) {
            $login_message = 'Username or Password must not be empty';
            return $login_message;
        }

        $query = "SELECT *
				  FROM
				  admin_users
				  WHERE 
				  admin_user_name = '{$admin_user_name}'
				  AND 
				  admin_pass = '{$admin_pass}'";

        $select_query = $this->db->select_data($query);

        if ($select_query != false) {
            $row = $select_query->fetch_assoc();

            Session::set('login', true);
            Session::set('adminId', $row['admin_id']);
            Session::set('adminUserName', $row['admin_user_name']);
            Session::set('admin_name', $row['admin_name']);
            header("Location:index.php");
        }
        $login_message = "Username or Password not matched!!!";
        return $login_message;
    }
}