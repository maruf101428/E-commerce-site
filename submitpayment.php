<?php
require_once 'includes/header.php';
require_once 'PaymentTransactions/charge-credit-card.php';
//require_once 'TransactionReporting/get-transaction-details.php';

$login = Session::get("customer_login");

if ($login == false) {
    header("Location:login.php");
}


if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {

    $customer_id = Session::get("customer_id");
    $insert_order_by_customer = $cart->order_product_by_customer($customer_id);
    $delete_cart_data = $cart->delete_customer_cart();
    $transaction_sms = chargeCreditCard($_POST);
    if ($transaction_sms != "Transaction Failed") {
        Session::set("Transaction_Id", $transaction_sms);
        header("Location:success.php");
    }
    echo $transaction_sms;
    //  header("Location:success.php");
}

$states = array('AL' => "Alabama", 'AK' => "Alaska", 'AZ' => "Arizona", 'AR' => "Arkansas", 'CA' => "California", 'CO' => "Colorado", 'CT' => "Connecticut"
, 'DE' => "Delaware", 'DC' => "District Of Columbia", 'FL' => "Florida", 'GA' => "Georgia", 'HI' => "Hawaii", 'ID' => "Idaho", 'IL' => "Illinois"
, 'IN' => "Indiana", 'IA' => "Iowa", 'KS' => "Kansas", 'KY' => "Kentucky", 'LA' => "Louisiana", 'ME' => "Maine", 'MD' => "Maryland"
, 'MA' => "Massachusetts", 'MI' => "Michigan", 'MN' => "Minnesota", 'MS' => "Mississippi", 'MO' => "Missouri", 'MT' => "Montana"
, 'NE' => "Nebraska", 'NV' => "Nevada", 'NH' => "New Hampshire", 'NJ' => "New Jersey", 'NM' => "New Mexico", 'NY' => "New York"
, 'NC' => "North Carolina", 'ND' => "North Dakota", 'OH' => "Ohio", 'OK' => "Oklahoma", 'OR' => "Oregon", 'PA' => "Pennsylvania"
, 'RI' => "Rhode Island", 'SC' => "South Carolina", 'SD' => "South Dakota", 'TN' => "Tennessee", 'TX' => "Texas", 'UT' => "Utah"
, 'VT' => "Vermont", 'VA' => "Virginia", 'WA' => "Washington", 'WV' => "West Virginia", 'WI' => "Wisconsin", 'WY' => "Wyoming");

?>
    <style>
        .division {
            width: 50%;
            float: left;
        }

        .tblone {
            width: 500px;
            margin: 0 auto;
            border: 2px solid #ddd;
        }

        .tblone tr td {
            text-align: justify;
        }

        .tbltwo {
            float: right;
            text-align: left;
            width: 60%;
            border: 2px solid #ddd;
            margin-right: 42px;
            margin-top: 10px;
        }

        .tbltwo tr td {
            text-align: justify;
            padding: 5px 10px;
        }
    </style>

    <div class="main">
        <?php
        if (isset($transaction_sms)) {
            echo "<span style='color: red'><?=$transaction_sms?></span>";
        }
        ?>
        <div class="content">
            <div class="section group">
                <div class="division">
                    <table class="tblone">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                        </tr>
                        <?php
                        $get_cart_product = $cart->get_cart_product();
                        if ($get_cart_product) {
                            $i = 0;
                            $sum = 0;
                            $qty = 0;
                            while ($row = $get_cart_product->fetch_assoc()) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $row['product_name'] ?></td>
                                    <td>Tk. <?= $row['price'] ?></td>
                                    <td><?= $row['quantity'] ?></td>
                                    <td>Tk.
                                        <?php
                                        $total = $row['price'] * $row['quantity'];
                                        echo $total;
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $qty = $qty + $row['quantity'];
                                $sum = $sum + $total;
                                ?>
                            <?php }
                        } ?>
                    </table>
                    <table class="tbltwo">
                        <tr>
                            <td>Quantity</td>
                            <td>:</td>
                            <td><?php echo $qty ?></td>
                        </tr>
                        <tr>
                            <td>Sub Total</td>
                            <td>:</td>
                            <td>TK. <?php echo $sum ?></td>
                        </tr>
                        <tr>
                            <td>VAT</td>
                            <td>:</td>
                            <td>TK. 10% ($<?php echo $vat = $sum * 0.1 ?>)</td>
                        </tr>
                        <tr>
                            <td>Grand Total</td>
                            <td>:</td>
                            <td>TK.<?php
                                $vat = $sum * 0.10;
                                $grand_total = $sum + $vat;
                                echo $grand_total;
                                ?></td>
                        </tr>
                    </table>
                </div>
                <div class="division">
                    <?php
                    $id = Session::get("customer_id");
                    $get_customer_data_by_id = $customer->get_customer_data_by_id($id);
                    if ($get_customer_data_by_id) {
                        while ($row = $get_customer_data_by_id->fetch_assoc()) {
                            ?>
                            <form action="" method="post">
                                <input type="hidden" name="credit_card_amount" id="credit_card_amount"
                                       autocomplete="off" maxlength="19" value="<?= $grand_total ?>">
                                <table class="tblone">
                                    <tr>
                                        <td colspan="3"><h2>Payment Information</h2></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_number">Credit Card Number</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_number" id="credit_card"
                                                   autocomplete="off" maxlength="19" value="4111111111111111" readonly>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="expiration_month">Expiration Date</label>
                                        </td>
                                        <td>
                                            <select name="credit_card_expiration_month" id="expiration_month" required>
                                                <option value="0"></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="credit_card_expiration_year" id="expiration_year" required>
                                                <option value="0"></option>
                                                <?php
                                                $current_year = date("Y");
                                                $stop_year = $current_year + 12;
                                                for ($year = $current_year; $year <= $stop_year; $year++) {
                                                    ?>
                                                    <option value="<?= $year ?>"><?= $year ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>

                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <label for="credit_card_cvv">Security Code</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_cvv" id="cvv" autocomplete="off"
                                                   value="" maxlength="4" required>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <img src="images/3digit.png" width="380" height="313"
                                                 alt="Three digit CVV number on the back of the credit card">
                                        </td>
                                        <td>
                                            <img src="images/4digit.png" width="380" height="313"
                                                 alt="Four digit CVV number on the front of the credit card">
                                        </td>
                                    </tr>
                                </table>

                                <table class="tbltwo">
                                    <tr>
                                        <td colspan="3"><h2>Billing Information</h2></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_first_name">Cardholder's First Name</label>
                                        </td>
                                        <td>
                                            <input type="" name="credit_card_first_name" id="cardholder_first_name"
                                                   maxlength="30" value="<?= $row['user_name'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_last_name">Cardholder's Last Name</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_last_name" id="cardholder_last_name"
                                                   maxlength="30" value="" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_billing_address">Billing Address</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_billing_address" id="billing_address"
                                                   maxlength="45" value="<?= $row['user_address'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_billing_address2">Suite/Apt #</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_billing_address2" id="billing_address2"
                                                   maxlength="45" value="" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_billing_city">City</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_billing_city" id="billing_city"
                                                   maxlength="25" value="<?= $row['user_city'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_billing_state">State</label>
                                        </td>
                                        <td>
                                            <select id="billing_state" name="credit_card_billing_state" required>
                                                <option value="0"></option>
                                                <?php
                                                foreach ($states as $state_abbr => $state_name) {
                                                    $selected = (isset($billing_state) && $billing_state === $state_abbr) ? ' selected="selected"' : '';
                                                    ?>
                                                    <option value="<?= $state_abbr ?>"<?= $selected ?>><?= $state_name ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="credit_card_billing_country">Country</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_billing_country" id="billing_country"
                                                   maxlength="5" value="<?= $row['user_country'] ?>" readonly>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="credit_card_billing_zip">Zip Code</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_billing_zip" id="billing_zip"
                                                   maxlength="5" value="<?= $row['user_zip'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_telephone">Telephone Number</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_telephone" id="telephone"
                                                   maxlength="20" value="<?= $row['user_phone_no'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_email">Email Address</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_email" id="email" maxlength="20"
                                                   value="<?= $row['user_email'] ?>" readonly>
                                        </td>
                                    </tr>
                                </table>

                                <table class="tblone">
                                    <tr>
                                        <td colspan="3"><h2>Shipping Information</h2></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_recipient_first_name">Recipient's First Name</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_recipient_first_name"
                                                   id="recipient_first_name" maxlength="30"
                                                   value="<?= $row['user_name'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_recipient_last_name">Recipient's Last Name</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_recipient_last_name"
                                                   id="recipient_last_name" maxlength="30" value="" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_shipping_address">Shipping Address</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_shipping_address" id="shipping_address"
                                                   maxlength="45" value="<?= $row['user_address'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_shipping_address2">Suite/Apt #</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_shipping_address2"
                                                   id="shipping_address2" maxlength="45" value="" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_shipping_city">City</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_shipping_city" id="shipping_city"
                                                   maxlength="30" value="<?= $row['user_city'] ?>" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_shipping_state">State</label>
                                        </td>
                                        <td>
                                            <select id="shipping_state" name="credit_card_shipping_state" required>
                                                <option value="0"></option>
                                                <?php
                                                foreach ($states as $state_abbr => $state_name) {
                                                    $selected = (isset($shipping_state) && $shipping_state === $state_abbr) ? ' selected="selected"' : '';
                                                    ?>
                                                    <option value="<?= $state_abbr ?>"<?= $selected ?>><?= $state_name ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="credit_card_shipping_zip">Zip Code</label>
                                        </td>
                                        <td>
                                            <input type="text" name="credit_card_shipping_zip" id="shipping_zip"
                                                   maxlength="5" value="<?= $row['user_zip'] ?>" readonly>
                                        </td>
                                    </tr>
                                </table>

                                <table class="tbltwo">
                                    <tr>
                                        <td>
                                            <input style="cursor:progress;background:#222;padding:10px 30px;border-radius:8px;color:#fff;margin:10px;font-size: 17px;"
                                                   type="submit" name="submit" value="Checkout">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        <?php }
                    } ?>
                </div>
                <div>
                    <a href="cart.php"
                       style="background:#222;padding:10px 30px;border-radius:4px;color:#fff;margin-top:10px;">Cart</a>
                </div>
            </div>
        </div>
    </div>
<?php require_once 'includes/footer.php';