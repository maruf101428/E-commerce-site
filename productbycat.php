<?php
require_once 'includes/header.php';
if (!isset($_GET['cat_id']) || $_GET['cat_id'] == NULL) {
    echo "<script>window.location = '404.php';</script>";
} else {
    $id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['cat_id']);
}
?>
    <div class="main">
        <div class="content">
            <div class="content_top">
                <div class="heading">
                    <h3>Latest from Category</h3>
                </div>
                <div class="clear"></div>
            </div>
            <div class="section group">
                <?php
                $product_by_category = $product->get_product_by_category_id($id);
                if ($product_by_category) {
                    while ($row = $product_by_category->fetch_assoc()) {
                        ?>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="details.php?prodid=<?= $row['product_id'] ?>"><img height="200px"
                                                                                        src="admin/<?= $row['product_image'] ?>"
                                                                                        alt=""/></a>
                            <h2><?= $row['product_name'] ?></h2>
                            <p><?= $fm->textShorten($row['product_name'], 60) ?></p>
                            <p><span class="price">$ <?= $row['price'] ?></span></p>
                            <div class="button"><span><a href="details.php?prodid=<?= $row['product_id'] ?>"
                                                         class="details">Details</a></span></div>
                        </div>
                    <?php }
                } else {
                    echo "<span class='error'>No product availavle under this Category !</span>";
                } ?>
            </div>
        </div>
    </div>
<?php require_once 'includes/footer.php';