<?php

class Format
{

    //Formating date by predefined process
    public function formatDate($date)
    {
        return date('F j, Y, g:i a', strtotime($date));
    }

    //For shortening any description
    public function textShorten($text, $limit = 400)
    {
        $text = $text . " ";
        $text = substr($text, 0, $limit);
        $text = substr($text, 0, strpos($text, ' '));
        $text = $text . "....";
        return $text;
    }

    //Formating some special characters
    public function validation($data)
    {
        $data = trim($data); //For removing whitespaces
        $data = stripcslashes($data); //For removing backslashes
        $data = htmlspecialchars($data);
        return $data;
    }

    //Locating the script name
    public function title()
    {
        $path = $_SERVER['SCRIPT_FILENAME'];
        $title = basename($path, '.php');
        if ($title == 'index') {
            $title = 'home';
        } elseif ($title == 'contact') {
            $title = 'contact';
        }
        return $title = ucfirst($title); // Uppercase the first character
    }
}