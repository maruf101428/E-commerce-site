<?php
require_once 'includes/header.php';

$login = Session::get("customer_login");
if ($login == false) {
    header("Location:login.php");
}

$customer_id = Session::get("customer_id");

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
    $update_customer_details = $customer->update_customer_details($_POST, $customer_id);
}
?>
    <style>
        .tblone {
            width: 550px;
            margin: 0 auto;
            border: 2px solid #ddd;
        }

        .tblone tr td {
            text-align: justify;
        }

        .tblone input[type="text"] {
            width: 400px;
            padding: 5px;
            font-size: 15px;
        }
    </style>
    <div class="main">
        <div class="content">
            <div class="section group">
                <?php
                $id = Session::get("customer_id");
                $get_customer_data_by_id = $customer->get_customer_data_by_id($id);
                if ($get_customer_data_by_id) {
                    while ($row = $get_customer_data_by_id->fetch_assoc()) {
                        ?>
                        <form action="" method="post">
                            <table class="tblone">
                                <?php
                                if (isset($update_customer_details)) { ?>
                                    <tr>
                                        <td colspan="2">
                                            <h2>
                                                <?= $update_customer_details ?>
                                            </h2>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                <tr>
                                    <td colspan="2"><h2>Update Profile Details</h2></td>
                                </tr>
                                <tr>
                                    <td width="20%">Name</td>
                                    <td><input type="text" name="user_name" value="<?= $row['user_name'] ?>"></td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td><input type="text" name="user_phone_no" value="<?= $row['user_phone_no'] ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><input type="text" name="user_email" value="<?= $row['user_email'] ?>"></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td><input type="text" name="user_address" value="<?= $row['user_address'] ?>"></td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td><input type="text" name="user_city" value="<?= $row['user_city'] ?>"></td>
                                </tr>
                                <tr>
                                    <td>Zipcode</td>
                                    <td><input type="text" name="user_zip" value="<?= $row['user_zip'] ?>"></td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td><input type="text" name="user_country" value="<?= $row['user_country'] ?>"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="submit" name="submit" value="Save"></td>
                                </tr>
                            </table>
                        </form>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
<?php require_once 'includes/footer.php';