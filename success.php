<?php
require_once 'includes/header.php';
require_once 'TransactionReporting/get-transaction-details.php';
$login = Session::get("customer_login");

if ($login == false) {
    header("Location:login.php");
}

$transaction_id = Session::get("Transaction_Id");

$transaction_response = getTransactionDetails($transaction_id);
//echo $transaction_response->getTransaction()->getCustomerAddressType();
$payment = $transaction_response->getTransaction()->getPayment();
$customer_unique_information = $transaction_response->getTransaction()->getCustomer();
$customer_information = $transaction_response->getTransaction()->getBillTO();
//echo '<pre>'.var_export($payment->getCreditCard()->getCardNumber(),true).'</pre>';
//echo '<pre>'.var_export($transaction_response->getTransaction(),true).'</pre>';
?>

    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="division">
                    <table class="tblone">
                        <tr>
                            <td colspan="3"><h2>Transaction Status</h2></td>
                        </tr>

                        <tr>
                            <td>
                                Transaction Id
                            </td>
                            <td>
                                <?= $transaction_response->getTransaction()->getTransId()?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Transaction Status
                            </td>
                            <td>
                                <?= $transaction_response->getTransaction()->getTransactionStatus()?>
                            </td>
                        </tr>

                    </table>
                    <table class="tblone">
                        <tr>
                            <td colspan="3"><h2>Payment Information</h2></td>
                        </tr>

                        <tr>
                            <td>
                                Credit Card Type
                            </td>
                            <td>
                                <?= $payment->getCreditCard()->getCardType()?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Credit Card Number
                            </td>
                            <td>
                                <?= $payment->getCreditCard()->getCardNumber()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Expiration Date
                            </td>
                            <td>
                                <?= $payment->getCreditCard()->getExpirationDate()?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Chraged Amount
                            </td>
                            <td>
                                <?= $transaction_response->getTransaction()->getAuthAmount()?>
                            </td>
                        </tr>
                    </table>

                    <table class="tblone">

                        <tr>
                            <td colspan="3"><h2>Customer Billing Information</h2></td>
                        </tr>

                        <tr>
                            <td>
                                Customer Name
                            </td>
                            <td>
                                <?= $customer_information->getFirstName()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer Address
                            </td>
                            <td>
                                <?= $customer_information->getAddress()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer City
                            </td>
                            <td>
                                <?= $customer_information->getCity()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer State
                            </td>
                            <td>
                                <?= $customer_information->getState()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer Zip Code
                            </td>
                            <td>
                                <?= $customer_information->getZip()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer Country
                            </td>
                            <td>
                                <?= $customer_information->getCountry()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer Email
                            </td>
                            <td>
                                <?= $customer_unique_information->getEmail()?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Customer Id
                            </td>
                            <td>
                                <?= $customer_unique_information->getId()?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php require_once 'includes/footer.php';