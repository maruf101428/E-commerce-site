<?php
ob_start();
require_once 'library/Session.php';
require 'library/Database.php';
require 'helpers/Format.php';

Session::init();
spl_autoload_register(function ($class) {
    require_once 'classes/' . $class . ".php";
});

$db = new Database();
$fm = new Format();
$product = new Product();
$category = new Category();
$cart = new Cart();
$customer = new Customer();

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: max-age=2592000");
?>
<!DOCTYPE HTML>
<head>
    <title>Store Website</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="js/jquerymain.js"></script>
    <script src="js/script.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript" src="js/nav-hover.js"></script>
    <script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Doppio+One' rel='stylesheet' type='text/css'>
    <style>
        .success {
            font-size: 20px;
            color: green;
            font-weight: bold;
        }

        .error {
            font-size: 20px;
            color: red;
            font-weight: bold;
        }

        input {
            color: #222222;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('#dc_mega-menu-orange').dcMegaMenu({rowItems: '4', speed: 'fast', effect: 'fade'});
        });
    </script>
</head>
<body>
<div class="wrap">
    <div class="header_top">
        <div class="logo">
            <a href="index.php"><img class="img-logo" src="images/logo.jpg" alt=""/></a>
        </div>
        <div class="header_top_right">
            <div class="search_box">
                <form>
                    <input type="text" value="Search for Products" onfocus="this.value = '';"
                           onblur="if (this.value == '') {this.value = 'Search for Products';}"><input type="submit"
                                                                                                       value="SEARCH">
                </form>
            </div>
            <div class="shopping_cart">
                <div class="cart">
                    <a href="cart.php" title="View my shopping cart" rel="nofollow">
                        <span class="cart_title">Cart</span>
                        <span class="no_product">
								<?php
                                $check_cart_table_data = $cart->check_cart_table_data();
                                if ($check_cart_table_data) {
                                    $sum = Session::get("sum");
                                    $qty = Session::get("qty");
                                    echo "$" . $sum . " QTY: " . $qty;
                                } else {
                                    echo "(empty)";
                                }
                                ?>
							</span>
                    </a>
                </div>
            </div>
            <?php
            if (isset($_GET['customer_id'])) {
                $delete_data = $cart->delete_customer_cart();
                Session::destroy();
            }
            ?>
            <div class="login">
                <?php
                $login = Session::get("customer_login");
                if ($login == false) { ?>
                    <a href="login.php">Login</a>
                <?php } else { ?>
                    <a href="?customer_id=<?= Session::get('customer_id') ?>">logout</a>
                <?php } ?>

            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="menu">
        <ul id="dc_mega-menu-orange" class="dc_mm-orange">
            <li><a href="index.php">Home</a></li>
            <?php
            $check_cart_table_data = $cart->check_cart_table_data();
            if ($check_cart_table_data) { ?>
                <li><a href="cart.php">Cart</a></li>
                <li><a href="submitpayment.php">Payment</a></li>
            <?php } ?>

            <?php
            $customer_id = Session::get("customer_id");
            $check_order = false;//$cart->check_customer_order_by_id($customer_id);
            if ($check_order) { ?>
                <li><a href="orderdetails.php">Order</a></li>
            <?php } ?>

            <?php
            $login = Session::get("customer_login");
            if ($login == true) { ?>
                <li><a href="profile.php">Profile</a></li>
            <?php } ?>
            <li><a href="contact.php">Contact</a></li>
            <div class="clear"></div>
        </ul>
    </div>
