<?php require_once 'includes/header.php';

$login = Session::get("customer_login");

if ($login == false) {
    header("Location:login.php");
}
?>
    <style>
        .tblone {
            width: 550px;
            margin: 0 auto;
            border: 2px solid #ddd;
        }

        .tblone tr td {
            text-align: justify;
        }
    </style>
    <div class="main">
        <div class="content">
            <div class="section group">
                <?php
                $id = Session::get("customer_id");
                $getcustomer_data_by_id = $customer->get_customer_data_by_id($id);
                if ($getcustomer_data_by_id) {
                    while ($row = $getcustomer_data_by_id->fetch_assoc()) {
                        ?>
                        <table class="tblone">
                            <tr>
                                <td colspan="3"><h2>User Profile Details</h2></td>
                            </tr>
                            <tr>
                                <td width="20%">Name</td>
                                <td width="5%">:</td>
                                <td><?= $row['user_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td><?= $row['user_phone_no'] ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td><?= $row['user_email'] ?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td><?= $row['user_address'] ?></td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>:</td>
                                <td><?= $row['user_city'] ?></td>
                            </tr>
                            <tr>
                                <td>Zipcode</td>
                                <td>:</td>
                                <td><?= $row['user_zip'] ?></td>
                            </tr>
                            <tr>
                                <td>Country</td>
                                <td>:</td>
                                <td><?= $row['user_country'] ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><a href="editprofile.php">Update Details</a></td>
                            </tr>
                        </table>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
<?php require_once 'includes/footer.php' ?>